const config = module.exports;

config.port = 1234;

config.database = {
    mysql: {
        host: "localhost",
        user: "root",
        password: "123456",
        database: "library", // library 数据库
        port: 3306,
    },
};