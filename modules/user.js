const mysql = require('mysql');
const config = require('../config');
const database = mysql.createPool(config.database.mysql);
const commands = {
    checkLogin: 'select id from user where name=? and password=?',
    getUsers: 'select * from user',
    getUserByID: 'select * from user where id=?'
}

const user = {
    login: function(req, res, next) {
        const param = req.body || req.query || req.params;

        database.getConnection(function(err, connection) {
            if (err) {
                console.log("数据库连接失败");
            } else {
                connection.query(commands.checkLogin, [param.username, param.password], function(err, row) {
                    if (err) {
                        res.send(err);
                    } else {
                        if (row.length === 1) {
                            res.json('登录成功');
                        } else {
                            res.json('账户或密码错误');
                        }
                    }
                })
            }
        })
    }
}

module.exports = user