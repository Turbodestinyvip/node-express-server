const express = require('express');
const router = express.Router();
const user = require('../modules/user')
    /**
     * @typedef User
     * @property {string} id.required - 用户ID
     * @property {string} username.required - 用户名称
     * @property {string} email - 用户邮箱
     * @property {string} tel - 联系方式
     */

/**
 * 用户登录
 * @route POST /user/login
 * @group 用户 - 用户相关操作
 * @param {string} username.query.required - 用户名.
 * @param {string} password.query.required - 用户密码.
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 */
router.post('/login', function(req, res, next) {
    user.login(req, res, next);
})

/**
 * 添加用户
 * @route POST /user
 * @group 用户 - 用户相关操作
 * @param {User.model} user.body.required
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 */
router.post('/', function(req, res, next) {
    res.json('登录');
})

/**
 * 查询用户
 * @route GET /user
 * @group 用户 - 用户相关操作
 * @param {string} email.query.required - username or email - eg: user@domain
 * @param {string} password.query.required - user's password.
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 */
router.get('/', function(req, res, next) {
    res.json('查询');
})

/**
 * 更新用户
 * @route PUT /user
 * @group 用户 - 用户相关操作
 * @param {User.model} user.boby.required - username or email.
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 */
router.put('/', function(req, res, next) {
    res.json('修改');
})

/**
 * 删除用户
 * @route DELETE /user
 * @group 用户 - 用户相关操作
 * @param {string} id.query.required - 用户ID
 * @returns {boolean} 200 - 删除成功
 * @returns {Error}  default - Unexpected error
 */
router.delete('/', function(req, res, next) {
    res.json('删除');
})

module.exports = router